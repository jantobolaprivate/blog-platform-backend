var username = "blog-platform";
var password = "admin";

print("------------------------------------------------------------------------------------");
print("PART 1: Create Users");
print("------------------------------------------------------------------------------------");

db.createUser({  
   user:username,
   pwd:password,
   roles:[  
      {  
         role: "readWrite",
         db: "blog-platform"
      },
      { 
      	 role: "userAdminAnyDatabase",
      	 db: "admin" 
      },
      {  
         role: "userAdmin",
         db: "admin"
      },
      {  
         role: "dbAdmin",
         db: "admin"
      },
      {  
         role: "clusterAdmin",
         db: "admin"
      }
   ]
});

print("Users created.");
print("------------------------------------------------------------------------------------");