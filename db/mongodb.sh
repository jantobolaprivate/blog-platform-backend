#!/bin/bash

DATABASE="blog-platform"
USERNAME="blog-platform"
PASSWORD="admin"

MY_PATH="`dirname \"$0\"`"

RESULT=""

function checkState {
	mongo --eval "db.stats()" &>/dev/null
	RESULT=$?
}

function connectDatabase {
	if [[ $# -eq 0 ]] ; then
	    mongo "$DATABASE" -u "$USERNAME" -p "$PASSWORD"
	else
		mongo "$DATABASE" -u "$USERNAME" -p "$PASSWORD" --eval {$1}
	fi
}

function startDatabase {
	checkState

	if [ "$RESULT" -ne 0 ]; then
	    echo "mongodb is not running, starting now..."

	    DEFAULT_DATA="$MY_PATH/data"

		if [ ! -d "$DEFAULT_DATA" ]; then
			mkdir "$DEFAULT_DATA"
		fi

		if [ "$1" = "auth" ]; then			
			nohup mongod --dbpath "$DEFAULT_DATA" --auth &>/dev/null &
		else
			nohup mongod --dbpath "$DEFAULT_DATA" &>/dev/null &
		fi

		sleep 2
		echo "database should be up & running"
	else
	    echo "mongodb is running!"
	fi
}

function stopDatabase {
	checkState

	if [ "$RESULT" -ne 0 ]; then
	    echo "mongodb is not running."
	    exit 0
	else
	    echo "stopping mongodb..."

	    if [ "$1" = "auth" ]; then
			mongo admin -u "$USERNAME" -p "$PASSWORD" --authenticationDatabase "$DATABASE" --eval "db.shutdownServer()"
		else
			mongo admin --eval "db.shutdownServer()"
		fi

		sleep 1
	fi
}

if [ "$1" = "install" ]; then

	startDatabase
	echo "************************************************************************************"
	echo "***************************** INSTALLING MONGODB ***********************************"
	echo "************************************************************************************"
	echo "loading install.js"
	mongo $DATABASE --eval "load(\"$MY_PATH/install.js\");"
	stopDatabase
	wait %1
	echo "configuring authentication"
	startDatabase "auth"
	mongo $DATABASE -u "$USERNAME" -p "$PASSWORD" --eval "db.auth(\"$USERNAME\", \"$PASSWORD\");"
	stopDatabase "auth"
	wait %1
	echo "************************************************************************************"
	echo "**************************** INSTALLATION COMPLETE *********************************"
	echo "************************************************************************************"

elif [ "$1" = "start" ]; then

	startDatabase "auth"
   
elif [ "$1" = "stop" ]; then

	stopDatabase "auth"

elif [ "$1" = "connect" ]; then
	checkState

	if [ "$RESULT" -ne 0 ]; then
		echo "database is down"
	else
		connectDatabase
	fi

elif [ "$1" = "collection" ]; then

	if [ -n "$2" ]; then
		connectDatabase "db."$2".find().pretty()"
	else
		connectDatabase "printjson(db.getCollectionNames())"
	fi

fi