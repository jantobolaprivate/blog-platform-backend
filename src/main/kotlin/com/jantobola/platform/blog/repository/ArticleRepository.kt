package com.jantobola.platform.blog.repository

import com.jantobola.platform.blog.entity.Article
import org.springframework.data.mongodb.repository.MongoRepository

/**
 * ArticleRepository
 *
 * @author Jan Tobola, 2015
 */
interface ArticleRepository : MongoRepository<Article, String> {

}