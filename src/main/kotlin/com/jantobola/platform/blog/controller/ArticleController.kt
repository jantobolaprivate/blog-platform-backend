    package com.jantobola.platform.blog.controller

    import com.jantobola.platform.blog.entity.Article
    import com.jantobola.platform.blog.repository.ArticleRepository
    import org.apache.tomcat.jni.Thread
    import org.slf4j.Logger
    import org.slf4j.LoggerFactory
    import org.springframework.beans.factory.annotation.Autowired
    import org.springframework.http.HttpStatus
    import org.springframework.http.ResponseEntity
    import org.springframework.web.bind.annotation.PathVariable
    import org.springframework.web.bind.annotation.RequestMapping
    import org.springframework.web.bind.annotation.RequestMethod
    import org.springframework.web.bind.annotation.RestController

    @RestController
    class ArticleController {

        val log: Logger = LoggerFactory.getLogger(javaClass)

        @Autowired
        lateinit var articleRepository: ArticleRepository

        @RequestMapping("/articles")
        fun getArticles() : Any {
            return articleRepository.findAll()
        }

        @RequestMapping("/test")
        fun getTest() : Any {
            java.lang.Thread.sleep(2000)
            return ResponseEntity<String>(HttpStatus.BAD_REQUEST)
        }

        @RequestMapping(value = "/insert/{title}/{content}", method = arrayOf(RequestMethod.GET))
        fun insertArticle(@PathVariable title: String, @PathVariable content: String) : Any {

            val article = Article(title = title, content = content)
            val saved : Article? = articleRepository.save(article)

            saved?.id?.let { log.info("New entity saved [id:${article.id}]") }
            return "ok"
        }

    }
