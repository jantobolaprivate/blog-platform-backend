package com.jantobola.platform.blog.entity

import org.springframework.data.annotation.Id

/**
 * Article
 *
 * @author Jan Tobola, 2015
 */
data class Article(

    @Id
    var id: String? = null,

    var title: String = "",
    var content: String = ""
)