package com.jantobola.platform.blog

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.boot.context.web.SpringBootServletInitializer

@SpringBootApplication
open class BlogPlatformApplication : SpringBootServletInitializer() {

    override fun configure(application: SpringApplicationBuilder): SpringApplicationBuilder {
        return application.sources(BlogPlatformApplication::class.java);
    }

}

fun main(args: Array<String>) {
    SpringApplication.run(BlogPlatformApplication::class.java, *args)
}